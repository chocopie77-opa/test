#!/bin/bash

if ping $1 -c 1 > /dev/null; then
    result='Сайт доступен'
else
    result='Сайт недоступен'
fi

echo $result